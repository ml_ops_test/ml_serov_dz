import pandas as pd
import warnings
from sklearn.neighbors import LocalOutlierFactor
from sklearn.ensemble import IsolationForest

# игнорирование предрупреждений
warnings.filterwarnings("ignore")

# загрузка данных
data = pd.read_csv('banks.txt', sep=',', encoding='ISO-8859-1')

# создайте объект LOF с настройками по умолчанию
lof = LocalOutlierFactor()

# подгоняем объект LOF к данным и предсказываем аномалии
anomaly_labels = lof.fit_predict(data.iloc[:, 1:])

# идентифицируем аномалии
anomalies = data.iloc[:, 1:][anomaly_labels == -1]

# считаем количество аномалий
print("Number of anomalies:", len(anomalies))

# фильтрация аномалий с помощью LOF в режиме обнаружения новизны (Novelty = True)
lof_novelty = LocalOutlierFactor(novelty=True)
lof_novelty.fit(data.iloc[:, 1:])

anomaly_scores_novelty = lof_novelty.decision_function(anomalies)
anomaly_labels_novelty = lof_novelty.predict(anomalies)

# оценка количества совпадений
matches = sum(anomaly_labels_novelty == -1)
print("Number of matches:", matches)

# решаем задачу используя Isolation Forest
iforest = IsolationForest()
iforest.fit(data.iloc[:, 1:])

anomaly_scores_iforest = iforest.decision_function(data.iloc[:, 1:])
anomaly_labels_iforest = iforest.predict(data.iloc[:, 1:])

# импорт метрик качества, используемых при обнаружении аномалий
from sklearn.metrics import precision_score, recall_score, f1_score, roc_auc_score, average_precision_score

# расчет метрик для LOF
y_true = anomaly_labels
y_pred = anomaly_labels
precision = precision_score(y_true, y_pred)
recall = recall_score(y_true, y_pred)
f1 = f1_score(y_true, y_pred)
auc_roc = roc_auc_score(y_true, y_pred)
auc_pr = average_precision_score(y_true, y_pred)

print("LOF Metrics:")
print("Precision:", precision)
print("Recall:", recall)
print("F1-score:", f1)
print("AUC-ROC:", auc_roc)
print("AUC-PR:", auc_pr)

# расчет метрик для Isolation Forest
y_true = anomaly_labels_iforest
y_pred = anomaly_labels_iforest
precision_iforest = precision_score(y_true, y_pred)
recall_iforest = recall_score(y_true, y_pred)
f1_iforest = f1_score(y_true, y_pred)
auc_roc_iforest = roc_auc_score(y_true, y_pred)
auc_pr_iforest = average_precision_score(y_true, y_pred)

print("Isolation Forest Metrics:")
print("Precision:", precision_iforest)
print("Recall:", recall_iforest)
print("F1-score:", f1_iforest)
print("AUC-ROC:", auc_roc_iforest)
print("AUC-PR:", auc_pr_iforest)