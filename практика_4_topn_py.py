import pandas as pd

def read_dataset(file_path):
    dataset = pd.read_csv(file_path)
    return dataset

def top_n_products(dataset, n):
    top_products = dataset.groupby('product')['sales'].sum().sort_values(ascending=False).head(n)
    return top_products

dataset = read_dataset('sales_data.csv')

top_5_products = top_n_products(dataset, 5)
print(top_5_products)